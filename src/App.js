import React, { Component } from 'react';
//import logo from './logo.svg';
//import './App.css';
import List from './components/List';
import Add from './components/Add';
import Edit from './components/Edit';
import Delete from './components/Delete';

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      screen: 'list',
      id: 0
    }
  }

  onCancel() {
    this.setState(
      {
        screen: 'list'
      }
    )
  }

  onAddSelect() {
    this.setState(
      {
        screen: 'add'
      }
    )
  }

  onEditSelect(id) {
    this.setState(
      {
        screen: 'edit',
        id: id
      }
    )
  }

  onDeleteSelect(id) {
    this.setState(
      {
        screen: 'delete',
        id: id
      }
    )
  }

  onAddSubmit(data) {
    fetch("https://simple-contact-crud.herokuapp.com/contact", {
      method: 'post',
      body: JSON.stringify(data.body)
    })
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result)
        },
        (error) => {
          console.log(error)
        }
      )
      .then(
        this.setState(
          {
            screen: 'list'
          }
        )
      )
  }

  onEditSubmit(data) {
    fetch("https://simple-contact-crud.herokuapp.com/contact/" + data.id, {
      method: 'put',
      body: JSON.stringify(data.body)
    })
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result)
        },
        (error) => {
          console.log(error)
        }
      )
      .then(
        this.setState(
          {
            screen: 'list'
          }
        )
      )
  }

  onDeleteSubmit(id) {
    fetch("https://simple-contact-crud.herokuapp.com/contact/" + id, {
      method: 'delete'
    })
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result)
        },
        (error) => {
          console.log(error)
        }
      )
      .then(
        this.setState(
          {
            screen: 'list'
          }
        )
      )
  }

  render_screen() {
    switch (this.state.screen) {
      case 'list': {
        return <List
          onClickAdd={() => { this.onAddSelect() }}
          onClickEdit={(id) => { this.onEditSelect(id) }}
          onClickDelete={(id) => { this.onDeleteSelect(id) }}
        />
      }
        break;
      case 'add': {
        return <Add
          onClickCancel={() => { this.onCancel() }}
          onClickSubmit={(data) => { this.onAddSubmit(data) }}
        />
      }
        break;
      case 'edit': {
        return <Edit
          id={this.state.id}
          onClickCancel={() => { this.onCancel() }}
          onClickSubmit={(data) => { this.onEditSubmit(data) }}
        />
      }
        break;
      case 'delete': {
        return <Delete
          id={this.state.id}
          onClickCancel={() => { this.onCancel() }}
          onClickSubmit={(id) => { this.onDeleteSubmit(id) }}
        />
      }
        break;
    }
  }
  render() {
    return (
      <div>
        {this.render_screen()}
      </div>
      /*
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
        </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
        </a>
        </header>
      </div>
      */
    )
  }
}

export default App;
