import React, { Component } from 'react';



class List extends Component {

    constructor(props) {
        super(props)
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        fetch("https://simple-contact-crud.herokuapp.com/contact")
            .then(res => res.json())
            .then(
                (result) => {
                    console.log(result)
                    this.setState({
                        isLoaded: true,
                        items: result.data
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const { error, isLoaded, items } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {

            return (
                <div>
                    <button onClick={() => { this.props.onClickAdd() }}>add</button>
                    <ul>
                        {
                            items.map(item => (
                                <li key={item.id}>
                                    {item.firstName} {item.lastName} <button onClick={() => { this.props.onClickEdit(item.id) }}>Edit</button><button onClick={() => { this.props.onClickDelete(item.id) }}>Delete</button>
                                    <ul>
                                        <li>ID: {item.id}</li>
                                        <li>First Name: {item.firstName}</li>
                                        <li>Last Name: {item.lastName}</li>
                                        <li>Age: {item.age}</li>
                                        <li>Picture: {item.photo}</li>
                                    </ul>
                                </li>
                            ))
                        }
                    </ul>
                </div>
            );

        }

    }

}

export default List;