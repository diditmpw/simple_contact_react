import React, { Component } from 'react';


class Add extends Component {
    render() {
        return (
            <div>
                <form method="post"
                    onSubmit={(e) => {
                        e.preventDefault();
                        this.props.onClickSubmit(
                            {
                                id: 0,
                                body: {
                                    firstName: e.target.firstName.value,
                                    lastName: e.target.lastName.value,
                                    age: e.target.age.value,
                                    photo: e.target.photo.value,
                                }
                            }
                        )
                    }}
                >
                    <div>
                        First Name: <input id="firstName" name="firstName" type="text" required ></input>
                    </div>
                    <div>
                        Last Name: <input id="lastName" name="lastName" type="text" required ></input>
                    </div>
                    <div>
                        Age: <input id="age" name="age" type="number" min="1" required ></input>
                    </div>
                    <div>
                        Picture: <input id="photo" name="photo" type="text" required ></input>
                    </div>
                    <button onClick={() => { this.props.onClickCancel() }}>Cancel</button><button type="submit">Submit</button>
                </form>
            </div>
        )
    }
}

export default Add;