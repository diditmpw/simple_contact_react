import React, { Component } from 'react';


class Edit extends Component {
    constructor(props) {
        super(props)
        this.state = {
            error: null,
            isLoaded: false,
            item: {}
        };
    }

    componentDidMount() {
        fetch("https://simple-contact-crud.herokuapp.com/contact/" + this.props.id)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        item: result.data
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }



    render() {
        const { error, isLoaded, item } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div>
                    <form method="post"
                        onSubmit={(e) => {
                            e.preventDefault(); 
                            this.props.onClickSubmit(
                                {
                                    id: this.props.id,
                                    body: {
                                        firstName: e.target.firstName.value,
                                        lastName: e.target.lastName.value,
                                        age: e.target.age.value,
                                        photo: e.target.photo.value,
                                    }
                                }
                                )
                        }}
                    >
                        <div>
                            First Name: <input defaultValue={item.firstName} id="firstName" name="firstName" type="text" required ></input>
                        </div>
                        <div>
                            Last Name: <input defaultValue={item.lastName} id="lastName" name="lastName" type="text" required ></input>
                        </div>
                        <div>
                            Age: <input defaultValue={item.age} id="age" name="age" type="number"  ></input>
                        </div>
                        <div>
                            Picture: <input defaultValue={item.photo} id="photo" name="photo" type="text" ></input>
                        </div>
                        <button onClick={() => { this.props.onClickCancel() }}>Cancel</button><button type="submit">Submit</button>
                    </form>
                </div>
            )
        }
    }
}

export default Edit;