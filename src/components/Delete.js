import React, { Component } from 'react';


class Delete extends Component {
    constructor(props) {
        super(props)
        this.state = {
            error: null,
            isLoaded: false,
            item: {}
        };
    }

    componentDidMount() {
        fetch("https://simple-contact-crud.herokuapp.com/contact/" + this.props.id)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        item: result.data
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }


    render() {
        const { error, isLoaded, item } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div>
                    <form method="post"
                        onSubmit={(e) => {
                            e.preventDefault(); this.props.onClickSubmit(this.props.id)
                        }}
                    >
                        <div>
                            First Name: {item.firstName} 
                        </div>
                        <div>
                            Last Name: {item.lastName} 
                        </div>
                        <div>
                            Age: {item.age}  
                        </div>
                        <div>
                            Picture: {item.photo} 
                        </div>
                        <button onClick={() => { this.props.onClickCancel() }}>Cancel</button><button type="submit">Submit</button>
                    </form>
                </div>
            )
        }
    }
}

export default Delete;